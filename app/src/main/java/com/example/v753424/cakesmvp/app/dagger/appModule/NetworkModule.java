package com.example.v753424.cakesmvp.app.dagger.appModule;

import android.content.Context;

import com.example.v753424.cakesmvp.app.dagger.appQualifier.AppContext;
import com.example.v753424.cakesmvp.app.dagger.appScope.AppScope;

import java.io.File;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import timber.log.Timber;

/**
 * Created by v753424 on 7/26/2017.
 */

@Module(includes = ContextModule.class)
public class NetworkModule {

    @Provides
    @AppScope
    public File cacheFile(@AppContext Context context){
        return new File(context.getCacheDir(), "okhttp_cache");
    }

    @Provides
    @AppScope
    public Cache cache(File cacheFile){
        return new Cache(cacheFile, 10 * 1000 * 1000); // 10MB
    }

    @Provides
    @AppScope
    public HttpLoggingInterceptor httpLoggingInterceptor(){
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String message) {
                Timber.i(message);
            }
        });
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);
        return httpLoggingInterceptor;
    }

    @Provides
    @AppScope
    public OkHttpClient okHttpClient(HttpLoggingInterceptor httpLoggingInterceptor, Cache cache){
        return new OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor)
                .cache(cache)
                .build();
    }
}
