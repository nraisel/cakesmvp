package com.example.v753424.cakesmvp.screen.home;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.Explode;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import com.example.v753424.cakesmvp.R;
import com.example.v753424.cakesmvp.adapter.CakeAdapter;
import com.example.v753424.cakesmvp.app.CakeApplication;
import com.example.v753424.cakesmvp.base.BaseActivity;
import com.example.v753424.cakesmvp.infrastructure.model.CakeResponse;
import com.example.v753424.cakesmvp.mvp.model.Cake;
import com.example.v753424.cakesmvp.mvp.presenter.CakePresenter;
import com.example.v753424.cakesmvp.mvp.view.IMainView;
import com.example.v753424.cakesmvp.screen.home.Details.DetailActivity;
import com.example.v753424.cakesmvp.screen.home.dagger.homeComponent.DaggerHomeComponent;
import com.example.v753424.cakesmvp.screen.home.dagger.homeModule.HomedModule;
import com.example.v753424.cakesmvp.util.IClickListener;
import com.example.v753424.cakesmvp.util.RecyclerTouchListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import timber.log.Timber;

public class MainActivity extends BaseActivity implements IMainView {

    @Inject
    CakeAdapter cakeAdapter;

    @Inject
    Picasso picasso;

    @Inject
    CakePresenter cakePresenter;


    @Bind(R.id.recycler_view)
    protected RecyclerView recycler_view;

    private List<Cake> cakeList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        super.onCreate(savedInstanceState);

        setContentView(getContentView());
        ButterKnife.bind(this);
        Timber.i("onCreate");
        cakeList = new ArrayList<>();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setExitTransition(new Explode());
        }

        DaggerHomeComponent.builder()
                .homedModule(new HomedModule(this))
                .appComponent(CakeApplication.getCakeApplication(this).getComponent())
                .build().inject(this);

        ShowDialog("Loading all cakes...");
        //cakePresenter = new CakePresenter(MainActivity.this);
        cakePresenter.onCreate();
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_main;
    }

    @Override
    protected void onResume() {
        super.onResume();
        Timber.i("onResume");
        cakePresenter.onSubscribe();
        ShowCakeList();
        onRowClick();
        if (DetailActivity.getInstance() != null)
            DetailActivity.getInstance().finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Timber.i("onDestroy");
        ButterKnife.unbind(this);
        cakePresenter.onUnSubscribe();
    }

    private void ShowCakeList() {
        //cakeAdapter = new CakeAdapter(this, picasso);
        recycler_view.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recycler_view.setHasFixedSize(true);
        recycler_view.setItemAnimator(new DefaultItemAnimator());
        recycler_view.setAdapter(cakeAdapter);
    }

    private void onRowClick() {
        recycler_view.addOnItemTouchListener(new RecyclerTouchListener(MainActivity.this.getApplicationContext(),
                recycler_view, new IClickListener() {
            @Override
            public void onClick(View view, int position) {
                //Toast.makeText(MainActivity.this, "You clicked row " + position, Toast.LENGTH_SHORT).show();
                Intent i = new Intent(MainActivity.this, DetailActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        startActivity(i, ActivityOptions.makeSceneTransitionAnimation(MainActivity.this).toBundle());
                    }
                else*/
                    startActivity(i);

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }


    @Override
    public void onCompleted() {
        hideDialog();
        Toast.makeText(MainActivity.this, "Loading completed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(String message) {
        hideDialog();
        Toast.makeText(MainActivity.this, "Error loading all the cakes ... " + message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onNext(CakeResponse cakeResponse) {
        Timber.i("Log on NEXT");
        cakeAdapter.addCakes(cakeResponse.getCakes());
    }




    /*private void GetAllCakes() {
        //Observable<CakeResponse> result = cakeApiService.getCakes();
        Observable<CakeResponse> result = cakePresenter.GetAllCakes();
        Subscription subscription = result.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CakeResponse>() {
                    @Override
                    public void onCompleted() {
                        hideDialog();
                        Toast.makeText(MainActivity.this, "Loading completed", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(Throwable e) {
                        hideDialog();
                        Toast.makeText(MainActivity.this, "Error loading all the cakes ... " + e.getMessage(), Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onNext(CakeResponse cakeResponse) {
                        cakeAdapter.addCakes(cakeResponse.getCakes());
                    }
                });
        compositeSubscription.add(subscription);
    }*/


}
