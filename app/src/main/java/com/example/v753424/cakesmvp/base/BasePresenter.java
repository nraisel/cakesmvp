package com.example.v753424.cakesmvp.base;

import rx.Observable;
import rx.Observer;
import rx.subscriptions.CompositeSubscription;
import timber.log.Timber;

/**
 * Created by v753424 on 7/28/2017.
 */

public abstract class  BasePresenter {

    protected CompositeSubscription compositeSubscription;

    public void onCreate(){
        if (compositeSubscription == null || compositeSubscription.isUnsubscribed())
            compositeSubscription = new CompositeSubscription();
    }

    protected abstract <T> void Subscribe(Observable<T> observable, Observer<T> observer);

    public void onDestroy(){
        unSubscribeAll();
        Timber.i("onDestroy Executed", compositeSubscription);
    }

    protected void unSubscribeAll() {
        if (compositeSubscription != null) {
            compositeSubscription.unsubscribe();
            compositeSubscription.clear();
        }
    }
}
