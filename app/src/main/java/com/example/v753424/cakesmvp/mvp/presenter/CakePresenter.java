package com.example.v753424.cakesmvp.mvp.presenter;

import com.example.v753424.cakesmvp.api.CakeApiService;
import com.example.v753424.cakesmvp.base.BasePresenter;

import com.example.v753424.cakesmvp.infrastructure.model.CakeResponse;
import com.example.v753424.cakesmvp.mvp.model.CakeModel;
import com.example.v753424.cakesmvp.mvp.view.IMainView;
import com.example.v753424.cakesmvp.screen.home.MainActivity;

import javax.inject.Inject;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by v753424 on 7/28/2017.
 */

public class CakePresenter extends BasePresenter implements Observer<CakeResponse> {

    IMainView mainView;

    /*@Inject
    CakeApiService cakeApiService;*/

    CakeModel cakeModel;

    @Inject
    public CakePresenter(MainActivity mainView, CakeModel cakeModel) {
        this.mainView = mainView;
        this.cakeModel =cakeModel;
    }

    @Override
    public void onCompleted() {
        mainView.onCompleted();
    }

    @Override
    public void onError(Throwable e) {
        mainView.onError(e.getMessage());
    }

    @Override
    public void onNext(CakeResponse cakeResponse) {
        mainView.onNext(cakeResponse);
    }


    public void onSubscribe() {
        Observable<CakeResponse> result = cakeModel.get();//mainView.getCakeList();
        Subscribe(result, CakePresenter.this);
    }

    public void onUnSubscribe() {
        unSubscribeAll();
    }

    @Override
    protected <T> void Subscribe(Observable<T> observable, Observer<T> observer) {
        Subscription subscription = observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
        compositeSubscription.add(subscription);
    }

    // REPOSITORY PATTERN



}
