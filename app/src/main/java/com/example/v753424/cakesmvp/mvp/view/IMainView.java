package com.example.v753424.cakesmvp.mvp.view;

import com.example.v753424.cakesmvp.base.IBaseView;
import com.example.v753424.cakesmvp.infrastructure.model.CakeResponse;

import rx.Observable;

/**
 * Created by v753424 on 7/28/2017.
 */

public interface IMainView extends IBaseView {

    void onCompleted();

    void onError(String message);

    void onNext(CakeResponse cakeResponse);
}
