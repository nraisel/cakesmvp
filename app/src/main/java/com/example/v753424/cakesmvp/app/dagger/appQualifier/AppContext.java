package com.example.v753424.cakesmvp.app.dagger.appQualifier;


import javax.inject.Qualifier;

/**
 * Created by v753424 on 7/27/2017.
 */

@Qualifier
public @interface AppContext {
}
