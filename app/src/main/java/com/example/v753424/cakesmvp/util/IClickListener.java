package com.example.v753424.cakesmvp.util;

import android.view.View;

/**
 * Created by Raisel on 10/28/2016.
 */

public interface IClickListener {
    void onClick(View view, int position);

    void onLongClick(View view, int position);
}
