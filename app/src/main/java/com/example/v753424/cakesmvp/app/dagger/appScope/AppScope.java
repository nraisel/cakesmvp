package com.example.v753424.cakesmvp.app.dagger.appScope;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by v753424 on 7/26/2017.
 */
@Scope
@Retention(RetentionPolicy.CLASS)
public @interface AppScope {
}
