package com.example.v753424.cakesmvp.app.dagger.appModule;

import com.example.v753424.cakesmvp.api.CakeApiService;
import com.example.v753424.cakesmvp.app.dagger.appScope.AppScope;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by v753424 on 7/25/2017.
 */

@Module(includes = {NetworkModule.class, GsonModule.class})
public class ClientApiServiceModule {

    @Provides
    @AppScope
    public RxJavaCallAdapterFactory rxJava(){
        return RxJavaCallAdapterFactory.create();
    }

    @Provides
    @AppScope
    public Retrofit retrofit(GsonConverterFactory gson, RxJavaCallAdapterFactory rxJava, OkHttpClient okHttpClient){
        return new Retrofit.Builder()
                .addConverterFactory(gson)
                .addCallAdapterFactory(rxJava)
                .client(okHttpClient)
                .baseUrl("https://gist.githubusercontent.com")
                .build();
    }

    @Provides
    @AppScope
    public CakeApiService cakeApiService(Retrofit retrofit){
        return retrofit.create(CakeApiService.class);
    }

}
