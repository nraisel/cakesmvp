package com.example.v753424.cakesmvp.screen.home.dagger.homeComponent;

import com.example.v753424.cakesmvp.app.dagger.appComponent.AppComponent;
import com.example.v753424.cakesmvp.screen.home.MainActivity;
import com.example.v753424.cakesmvp.screen.home.dagger.homeModule.HomedModule;
import com.example.v753424.cakesmvp.screen.home.dagger.homeScope.HomeActivityScope;

import dagger.Component;

/**
 * Created by Raisel on 7/27/2017.
 */

@HomeActivityScope
@Component(modules = HomedModule.class, dependencies = AppComponent.class)
public interface HomeComponent {

    //CakeAdapter cakeAdapter();
    void inject(MainActivity activity);
}
