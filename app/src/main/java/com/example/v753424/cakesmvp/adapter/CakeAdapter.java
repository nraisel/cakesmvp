package com.example.v753424.cakesmvp.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.v753424.cakesmvp.R;
import com.example.v753424.cakesmvp.infrastructure.model.Cake;
import com.example.v753424.cakesmvp.screen.home.MainActivity;
import com.example.v753424.cakesmvp.util.CircleTransform;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Raisel on 7/27/2017.
 */

public class CakeAdapter extends RecyclerView.Adapter<CakeAdapter.Holder> {

    protected final MainActivity mainActivity;

    protected final Picasso picasso;

    private final List<Cake> cakeList = new ArrayList<>();


    @Inject
    public CakeAdapter(MainActivity mainActivity, Picasso picasso) {
        this.mainActivity = mainActivity;
        this.picasso = picasso;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cake_list_row, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.bind(cakeList.get(position));
    }

    @Override
    public int getItemCount() {
        return cakeList.size();
    }

    public void addCakes(List<Cake> cakes){
        cakeList.addAll(cakes);
        notifyDataSetChanged();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @Bind(R.id.cakeImg)
        protected ImageView cakeImage;
        @Bind(R.id.cakeTitle)
        protected TextView cakeTitle;
        @Bind(R.id.cakePreview)
        protected TextView cakePreviewDescription;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(Cake cake) {
            picasso.load(cake.getImage())
                    .transform(new CircleTransform())
                    .into(cakeImage);
            cakeTitle.setText(cake.getTitle().toString());
            cakePreviewDescription.setText(cake.getPreviewDescription().toString());
        }
    }
}
