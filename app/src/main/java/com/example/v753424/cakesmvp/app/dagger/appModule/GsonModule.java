package com.example.v753424.cakesmvp.app.dagger.appModule;


import com.example.v753424.cakesmvp.app.dagger.appScope.AppScope;
import com.google.gson.Gson;

import dagger.Module;
import dagger.Provides;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by v753424 on 7/30/2017.
 */
@Module
public class GsonModule {

    @Provides
    @AppScope
    public GsonConverterFactory gson(){
        return GsonConverterFactory.create();
    }
}
