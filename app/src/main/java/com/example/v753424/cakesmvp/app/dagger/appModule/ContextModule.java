package com.example.v753424.cakesmvp.app.dagger.appModule;

import android.content.Context;

import com.example.v753424.cakesmvp.app.dagger.appQualifier.AppContext;
import com.example.v753424.cakesmvp.app.dagger.appScope.AppScope;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Raisel on 7/26/2017.
 */

@Module
public class ContextModule {

    private Context context;

    public ContextModule(Context context) {
        this.context = context.getApplicationContext();
    }

    @Provides
    @AppScope
    @AppContext
    public Context getContext() {
        return context;
    }


}
