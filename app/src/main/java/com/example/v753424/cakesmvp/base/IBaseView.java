package com.example.v753424.cakesmvp.base;

/**
 * Created by v753424 on 7/28/2017.
 */

public interface IBaseView {

    void ShowDialog(String message);
    void hideDialog();
}
