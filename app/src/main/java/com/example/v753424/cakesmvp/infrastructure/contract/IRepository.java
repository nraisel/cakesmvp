package com.example.v753424.cakesmvp.infrastructure.contract;

import java.util.List;

import rx.Observable;

/**
 * Created by v753424 on 7/30/2017.
 */

public interface IRepository<T> {

    Observable<List<T>> getListOf();

    Observable<T> get();

    T getById(String Id);

    long add(T obj);

    long addAll(List<T> obj);

    int update(String Id, T obj);

    int delete(String Id);
}
