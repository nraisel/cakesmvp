package com.example.v753424.cakesmvp.app.dagger.appComponent;

import com.example.v753424.cakesmvp.api.CakeApiService;
import com.example.v753424.cakesmvp.app.CakeApplication;
import com.example.v753424.cakesmvp.app.dagger.appModule.ClientApiServiceModule;
import com.example.v753424.cakesmvp.app.dagger.appModule.PicassoModule;
import com.example.v753424.cakesmvp.app.dagger.appScope.AppScope;
import com.squareup.picasso.Picasso;

import dagger.Component;

/**
 * Created by Raisel on 7/25/2017.
 */

@AppScope
@Component(modules = {ClientApiServiceModule.class, PicassoModule.class})
public interface AppComponent {

    Picasso getPicasso();

    CakeApiService getCakeApiService();
    //void inject(CakeApplication application);
}
