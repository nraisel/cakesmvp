package com.example.v753424.cakesmvp.screen.home.dagger.homeModule;

import com.example.v753424.cakesmvp.mvp.model.CakeModel;
import com.example.v753424.cakesmvp.screen.home.MainActivity;
import com.example.v753424.cakesmvp.screen.home.dagger.homeScope.HomeActivityScope;

import dagger.Module;
import dagger.Provides;

/**
 * Created by v753424 on 7/27/2017.
 */

@Module
public class HomedModule {

    private MainActivity mainActivity;

    public HomedModule(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Provides
    @HomeActivityScope
    public MainActivity mainActivity(){
        return mainActivity;
    }
    /*public CakeAdapter cakeAdapter(Picasso picasso){
        return new CakeAdapter(mainActivity, picasso);
    }*/

    /*@Provides
    @HomeActivityScope
    public CakeModel cakeModel(){
        return new CakeModel();
    }*/
}
