package com.example.v753424.cakesmvp.screen.home.Details;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.Slide;
import android.view.Gravity;
import android.view.Window;
import android.view.animation.LinearInterpolator;

import com.example.v753424.cakesmvp.R;
import com.example.v753424.cakesmvp.screen.home.MainActivity;

import timber.log.Timber;

public class DetailActivity extends AppCompatActivity {

    private static DetailActivity detailActivity;
    public static DetailActivity getInstance() {
        return detailActivity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        detailActivity = DetailActivity.this;
        Timber.i("OnCreate_Details");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Slide slide = new Slide();
            slide.setInterpolator(new LinearInterpolator());
            slide.setSlideEdge(Gravity.LEFT);
            getWindow().setAllowEnterTransitionOverlap(true);
            getWindow().setEnterTransition(slide);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Timber.i("OnRestart_Details");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Timber.i("OnResume_Details");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Timber.i("OnPause_Details");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Timber.i("OnDestroy_Details");
        DetailActivity.this.finish();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        DetailActivity.this.finish();
    }
}
