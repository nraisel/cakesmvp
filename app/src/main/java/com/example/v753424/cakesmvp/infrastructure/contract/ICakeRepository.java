package com.example.v753424.cakesmvp.infrastructure.contract;


import com.example.v753424.cakesmvp.infrastructure.model.CakeResponse;

/**
 * Created by v753424 on 7/30/2017.
 */

public interface ICakeRepository extends IRepository<CakeResponse> {
}
