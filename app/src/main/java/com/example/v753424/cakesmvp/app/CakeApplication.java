package com.example.v753424.cakesmvp.app;

import android.app.Activity;
import android.app.Application;

import com.example.v753424.cakesmvp.api.CakeApiService;
import com.example.v753424.cakesmvp.app.dagger.appComponent.AppComponent;
import com.example.v753424.cakesmvp.app.dagger.appComponent.DaggerAppComponent;
import com.example.v753424.cakesmvp.app.dagger.appModule.ContextModule;
import com.squareup.picasso.Picasso;


import timber.log.Timber;

/**
 * Created by Raisel on 7/25/2017.
 */

public class CakeApplication extends Application {

    protected Picasso picasso;

    protected CakeApiService cakeApiService;

    private AppComponent component;

    @Override
    public void onCreate() {
        super.onCreate();

        Timber.plant(new Timber.DebugTree());

        component = DaggerAppComponent.builder()
                .contextModule(new ContextModule(this))
                .build();

        picasso = component.getPicasso();
        cakeApiService = component.getCakeApiService();

    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    public AppComponent getComponent() {
        return component;
    }

    public Picasso getPicasso() {
        return picasso;
    }

    public CakeApiService getCakeApiService() {
        return cakeApiService;
    }

    public static CakeApplication getCakeApplication(Activity activity){
        return (CakeApplication) activity.getApplication();
    }
}
