package com.example.v753424.cakesmvp.app.dagger.appModule;

import android.content.Context;

import com.example.v753424.cakesmvp.app.dagger.appQualifier.AppContext;
import com.example.v753424.cakesmvp.app.dagger.appScope.AppScope;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;

/**
 * Created by Raisel on 7/26/2017.
 */

@Module(includes = NetworkModule.class)
public class PicassoModule {

    @Provides
    @AppScope
    public Picasso picasso(@AppContext Context context, OkHttp3Downloader okHttp3Downloader){
        return new Picasso.Builder(context)
                .downloader(okHttp3Downloader)
                .build();
    }

    @Provides
    @AppScope
    public OkHttp3Downloader okHttp3Downloader(OkHttpClient okHttpClient){
        return new OkHttp3Downloader(okHttpClient);
    }
}
