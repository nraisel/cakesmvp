package com.example.v753424.cakesmvp.mvp.model;

import com.example.v753424.cakesmvp.api.CakeApiService;
import com.example.v753424.cakesmvp.infrastructure.contract.ICakeRepository;
import com.example.v753424.cakesmvp.infrastructure.model.*;
import com.example.v753424.cakesmvp.infrastructure.model.CakeResponse;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by v753424 on 7/30/2017.
 */

public class CakeModel implements ICakeRepository {

    @Inject
    CakeApiService cakeApiService;

    /*@Inject
    public CakeModel(CakeApiService cakeApiService) {
        this.cakeApiService = cakeApiService;
    }*/

    @Inject
    public CakeModel() {
    }

    @Override
    public Observable<List<CakeResponse>> getListOf() {
        return null;
    }

    @Override
    public Observable<CakeResponse> get() {
        return cakeApiService.getCakes();
    }

    @Override
    public CakeResponse getById(String Id) {
        return null;
    }

    @Override
    public long add(CakeResponse obj) {
        return 0;
    }

    @Override
    public long addAll(List<CakeResponse> obj) {
        return 0;
    }

    @Override
    public int update(String Id, CakeResponse obj) {
        return 0;
    }

    @Override
    public int delete(String Id) {
        return 0;
    }
}
