package com.example.v753424.cakesmvp.screen.home.dagger.homeScope;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by v753424 on 7/27/2017.
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface HomeActivityScope {
}
